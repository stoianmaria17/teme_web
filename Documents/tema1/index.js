
const FIRST_NAME = "Maria";
const LAST_NAME = "Stoian";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function numberParser(value) 
{
    if(typeof value === 'string')
        return parseInt(value,10);
    if(typeof value ==='number'&& value==value &&Number.isFinite(value))
    {
        if((value<Number.MIN_SAFE_INTEGER || value>Number.MAX_SAFE_INTEGER))
        {
            return NaN;
        }
        else
            return parseInt(value);
        //return Math.floor(value);//parseInt(value);// return Math.trunc(value);    
    }
    return NaN;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

